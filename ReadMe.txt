-------------------------------------------------------------------------------------------------
 
guiX Version 0.2.2 BY Chris B. Bensler
A graphical user interface for exoticaX applications.


If you have any problems using guiX for ExoticaX, be sure to let me know!

                bensler@mail.com


guiX is 100% free!

terms of use:

-This must be included in your program and documentation:

                  guiX (c)2000 - 2002 Chris B. Bensler

-It would also be nice to get a copy of any programs made using guiX. :)


disclaimer:
-These routines work on my system, however, I cannot guarantee they will
 work on yours. I will not be held responsible for any damage these routines
 may cause to your system. Although I don't see any reason they would.

-- Chris B. Bensler                bensler@mail.com
 
-------------------------------------------------------------------------------------------------
-- guiX features:

. Skinnable objects
. Event handling
. As simple as win32lib 

-------------------------------------------------------------------------------------------------
GuiX provides the essential graphical objects needed to produce almost any control you might need.

The TITLE_BAR object is a new addition to the library, and is not fully integrated yet. However, it works well.
The problem with the TITLE_BAR object is that you must create a window without graphics, and define a panel to mimic the main display. Otherwise, you will have a titlebar that overlaps the window, instead of sitting on the top edge of the it.

-------------------------------------------------------------------------------------------------
The fate of guiX:

I attempted to make guiX user expandable. To this extent, I think I have failed :(
I think I succeeded in making the objects fairly independent. Adding a new object should be fairly simple.
On the other hand, adding a new event type, even for me, is difficult :/

Eventhough I am quite happy with the outcome of GuiX, and it's very productive, it will probably never make it to v1.0. 

I have a better concept on the backburner. I will most likely be using euGL, instead of exotica/X. I have yet to explore euGL though, so it's all up in the air right now. The new gui will utilize OOP, and will be devised in a way that both objects and events can be easily added. Each object will be it's own include file, keeping the engine separate.
I'm positive that the new concept will prove much more future proof than the last.

-------------------------------------------------------------------------------------------------
